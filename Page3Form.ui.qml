import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12
import QtQuick.Dialogs 1.0

Page {
    id: page3Form
    width: 600
    height: 400


    Rectangle {

    AnimatedImage {
        id: animatedImage
        x: 8
        y: 8
        width: 55
        height: 55
        source: "instagram.png"
    }

    Label {
        x: 16
        y: 0
        text: qsTr("         Лаб2 Запись и \n         воспроизведение \n         фото и видео\n")
        font.pixelSize: 23
        styleColor: "#000000"
        padding: 10

    }
    id: rectangleHeader
    x: 0
    y: 0
    width: 600
    height: 95
    anchors.right: parent.right
    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#f5c16c"
        }

        GradientStop {
            position: 1
            color: "#8e2a07"
        }
    }
    anchors.left: parent.left
}

    Rectangle{
        id: screen
        anchors.left: parent.left
        anchors.leftMargin: 25
        anchors.right: parent.right
        anchors.rightMargin: 25
        anchors.top: rectangleHeader.bottom
        anchors.topMargin: 25
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 58
        color: "black"

        Camera {
            id: camera
            imageCapture {
                onImageCaptured: {
                    photoPreview.source = preview
                }
            }
        }

        VideoOutput {
            source: camera
            focus : visible
            anchors.fill: parent
            autoOrientation: true

        }

        Image {
            id: photoPreview
            anchors.right: parent.right
            width: 192
            height: 108
        }
    }

        Button{
            id:photo
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 25
            anchors.rightMargin: 25
            anchors.bottom: parent.bottom
            anchors.top: screen.bottom
            anchors.bottomMargin: 0
            anchors.horizontalCenter: parent.horizontalCenter
            Material.accent: "#780c9a"
            onClicked: camera.imageCapture.capture()


            Text{
                text: "Сделать фото"
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 20
                font.family: "Tahoma"
                anchors.verticalCenterOffset: 1
                anchors.horizontalCenterOffset: 1
            }
            background: Rectangle
            {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                color: "#ecbe82"
            }
    }
}
