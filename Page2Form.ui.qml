import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12
import QtQuick.Dialogs 1.0

Page {
    id: page2Form
    width: 600
    height: 400


    header:

        Rectangle {

        AnimatedImage {
            id: animatedImage
            x: 8
            y: 8
            width: 55
            height: 55
            source: "instagram.png"
        }

        Label {
            x: 16
            y: 0
            text: qsTr("         Лаб2 Запись и \n         воспроизведение \n         фото и видео\n")
            font.pixelSize: 23
            styleColor: "#000000"
            padding: 10

        }
        id: rectangleHeader
        x: 0
        y: 0
        width: 600
        height: 95
        anchors.right: parent.right
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#f5c16c"
            }

            GradientStop {
                position: 1
                color: "#8e2a07"
            }
        }
        anchors.left: parent.left
    }


    Rectangle{
        id: screen
        anchors.left: parent.left
        anchors.leftMargin: 25
        anchors.right: parent.right
        anchors.rightMargin: 25
        anchors.top: parent.top
        anchors.topMargin: 25
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 150
        color: "black"

        MediaPlayer {
            id: mediaplayer
            source: "drake.mp4"
            volume: volumeSlider.volume
            loops: MediaPlayer.Infinite
        }

        VideoOutput {
            source: mediaplayer
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }
    }

    GridLayout{
        rows: 2
        columns: 3

        anchors.top: screen.bottom
        anchors.left: parent.left
        anchors.leftMargin: 25
        anchors.right: parent.right
        anchors.rightMargin: 25



        Slider{
            id: videoSlider
            Layout.row: 0
            Layout.column: 0
            Layout.columnSpan: 3
            Layout.fillWidth: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.left: parent.left
            anchors.right: parent.right

            to: mediaplayer.duration
            value: mediaplayer.position

            onPressedChanged: {
                mediaplayer.seek(videoSlider.value)
            }
            Material.accent: "#ecbe82"
        }



        Text{
           id:time
           Layout.row: 1
           Layout.column: 0
            anchors.left: parent.left
            anchors.leftMargin: 13

            font.pointSize: 14
            color: "#ecbe82"
            text: Qt.formatTime(new Date(mediaplayer.position), "mm:ss")

        }

        Button{
        Layout.margins: 10
        Layout.column: 1
        Layout.row: 1
        id:button
        enabled: mediaplayer.hasVideo
        Layout.preferredWidth: button.implicitHeight
        text: mediaplayer.playbackState === MediaPlayer.PlayingState ? "||" : "►"
        onClicked: mediaplayer.playbackState === MediaPlayer.PlayingState ? mediaplayer.pause() : mediaplayer.play()
        anchors.left: time.right
        anchors.right: volumeSlider.left
        anchors.rightMargin: 10
        anchors.leftMargin: 20

        background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        color: "#D2691E"
        border.color: control.down ? "#D2691E" : "#D2691E"
        border.width: 1
        radius: 5
        }
        }


        Slider{
            id: volumeSlider
            Layout.row: 1
            Layout.column: 2
            Layout.columnSpan: 2
            from: 0
            to: 1
            value: 0.2

            Material.accent: "#ecbe82"

            Layout.fillWidth: true
            anchors.right: parent.right

            property real volume: QtMultimedia.convertVolume(volumeSlider.value, QtMultimedia.LogarithmicVolumeScale, QtMultimedia.LinearVolumeScale)
        }
    }
}
