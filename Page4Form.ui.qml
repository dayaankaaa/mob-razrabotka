import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtMultimedia 5.4
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.12
import QtQuick.Controls.Material 2.3

Page{
    id: page4GUI
    width: 600
    height: 400
header:
    Rectangle {

    AnimatedImage {
        id: animatedImage
        x: 8
        y: 8
        width: 55
        height: 55
        source: "instagram.png"
    }

    Label {
        x: 16
        y: 0
        text: qsTr("         Лабораторная работа №3 \n          Запросы к серверу по  \n        протоколу HTTP/HTTPS")
        font.pixelSize: 23
        styleColor: "#000000"
        padding: 10

    }
    id: rectangleHeader
    x: 0
    y: 0
    width: 600
    height: 95
    anchors.right: parent.right
    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#f5c16c"
        }

        GradientStop {
            position: 1
            color: "#8e2a07"
        }
    }
    anchors.left: parent.left
}

    Rectangle {
        id: rectangle122
        color: "#1d2027"
        anchors.topMargin: 0
        anchors.fill: parent
    }
    GridLayout {
        anchors.topMargin: 82
        anchors.fill: parent
        columns: 2

        Button {
            id: sent
            anchors.top: rectangleHeader.bottom
            anchors.bottom: scrollView1.top
            anchors.bottomMargin: 40
            Layout.alignment: Qt.AlignCenter
            Layout.columnSpan: 2

            onClicked: {
                _send.getNetworkValue();
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 14
                text: qsTr("Запросить")
                color: "white"
            }
            background: Rectangle {
                implicitWidth: 200
                implicitHeight: 60
                color: "#ecbe82"
                radius: 5
            }
        }


        ScrollView {
            id: scrollView1
            Layout.fillHeight: true
            Layout.columnSpan: 2
            Layout.fillWidth: true

            clip:  true
            TextArea{
                id: textArea
                textFormat: Text.RichText
                objectName: "textArea"
                readOnly: true
                anchors.fill: parent
                background: Rectangle {
                    color: "#ecbe82"
                }
            }
        }

        Label {
            Layout.alignment: Qt.AlignCenter
            //Layout.fillWidth: true
            Layout.columnSpan: 2
            font.pointSize: 12
            text: "<b>Погода сегодня<b>"
            color: "#ecbe82"
        }
        RowLayout{
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignCenter

            TextField {
                id: textField
                objectName: "textField"
                color: "#ecbe82"
                horizontalAlignment: Text.AlignHCenter
                readOnly: true
                Layout.alignment: Qt.AlignCenter
                Layout.columnSpan: 2
                font.pixelSize: 16
            }
        }
    }
}
