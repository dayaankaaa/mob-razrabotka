#ifndef QHTTPCONTROLLER_H
#define QHTTPCONTROLLER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QCryptographicHash>

class HttpController : public QObject{
    Q_OBJECT
public:
    explicit HttpController(QObject *parent = nullptr);
    QNetworkAccessManager *nam; // полученный access_token

signals:
public slots:
    void onNetworkValue(QNetworkReply *reply);
    void getNetworkValue();

protected:
 QObject *showInfo;
};

#endif // QHTTPCONTROLLER_H
