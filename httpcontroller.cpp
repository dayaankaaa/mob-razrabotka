#include "httpcontroller.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

HttpController::HttpController(QObject *QMLObject) : showInfo(QMLObject){
    nam = new QNetworkAccessManager(this); // создаем Network Access Manager
    connect(nam, &QNetworkAccessManager::finished, this, &HttpController::onNetworkValue);
}

void HttpController::getNetworkValue(){
    QNetworkRequest request;
    request.setUrl(QUrl("https://www.gismeteo.ru/"));
    //qDebug() << request.url();
    QNetworkReply * reply;
    QEventLoop evntLoop;
    connect(nam, &QNetworkAccessManager::finished, &evntLoop, &QEventLoop::quit);
    reply = nam->get(request);
    evntLoop.exec();
    QString replyString = reply->readAll();
}

void HttpController::onNetworkValue(QNetworkReply *reply){
     QString str = reply->readAll(); // записывем в str наш сайт

     QObject* textField = showInfo->findChild<QObject*>("textField"); // ищем элемент, куда будем записывать значение курса
     QObject* textArea = showInfo->findChild<QObject*>("textArea"); // ищем элемент, куда будем записывать содержимое страницы

     textArea->setProperty("text", str); // записываем в параметр текст строку, в которой вся страница

     int i = 0;
     QString a; //gradusy

     if((i = str.indexOf("unit unit_temperature_c", i)) != -1)
       {
          a = str.mid(i + 78,2) + "°";
          textField->setProperty("text", a);
       }

}
