import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12
import QtQuick.Dialogs 1.0

Page {
    width: 600
    height: 400

    header:

        Rectangle {

        AnimatedImage {
            id: animatedImage
            x: 8
            y: 8
            width: 55
            height: 55
            source: "instagram.png"
        }

        Label {
            x: 16
            y: 0
            text: qsTr("         Лаб1 Элементы \n         графического интерфейса\n")
            font.pixelSize: 23
            styleColor: "#000000"
            padding: 10

        }
        id: rectangleHeader
        x: 0
        y: 0
        width: 600
        height: 69
        anchors.right: parent.right
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#f5c16c"
            }

            GradientStop {
                position: 1
                color: "#8e2a07"
            }
        }
        anchors.left: parent.left
    }

    GridLayout{
        id: gridLayout
        anchors.rightMargin: 0
        anchors.bottomMargin: 9
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        rows: 3
        columns: 2

        Tumbler {
            id: tumbler
            x: 225
            y: 25
            currentIndex: 0
            visibleItemCount: 6
            wrap: true
            model: 10
            Layout.row: 0
            Layout.column: 0
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.top: rectangleHeader.bottom
             Material.accent: "#ecbe82"
        }

        Text {
            id: label5
            x: 89
            y: 85
            width: 30
            color: "#ecbe82"
            text: qsTr("Крутилка \nВертелка")
            anchors.left: tumbler.right
            anchors.right: busyIndicator.left
            font.pointSize: 12
            font.family: "Tahoma"
            font.pixelSize: 19
            anchors.topMargin: 75
            anchors.bottomMargin: 208

        }



        RangeSlider {
            id: rangeSlider
            x: 195
            y: 67
            first.value: 0.25
            second.value: 0.75
            Layout.row: 1
            Layout.column: 0
            anchors.left: parent.left
            anchors.leftMargin: 30
            Material.accent: "#D2691E"

        }




        Text {
            id: label6
            x: 28
            y: 248
            width: 73
            text: qsTr("Плохо             Хорошо")
            anchors.left: rangeSlider.left
            anchors.right: button.left
            anchors.bottom: rangeSlider.top
            font.pointSize: 12
            font.family: "Tahoma"
            font.pixelSize: 20
            anchors.topMargin: 81
            color: "#ecbe82"
        }

    BusyIndicator {
        id: busyIndicator
        x: 438
        y: 121
        width: 70
        height: 78
        Layout.row: 0
        Layout.column: 1
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.top: rectangleHeader.bottom
        font.pointSize: 8
         Material.accent: "#ecbe82"
        background: Rectangle
        {
            anchors.fill:parent
            color: "#D2691E"
            radius: 10
        }
    }





        Button {
            id: button
            x: 189
            y: 206
            text: qsTr("Кнопка")
            Layout.row: 1
            Layout.column: 1
            anchors.right: parent.right
            font.pointSize: 13
            anchors.rightMargin: 20

            background: Rectangle {
            implicitWidth: 100
            implicitHeight: 40
            color: "#D2691E"
            border.width: 1
            radius: 5
            }
        }

        TextArea {
            id: textArea
            x: 240
            placeholderText: qsTr("Какой-то текст")
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.left: parent.left
            font.pointSize: 14
            anchors.top: rangeSlider.bottom
            anchors.topMargin: 15
            anchors.rightMargin: 20
             anchors.leftMargin: 20
            Layout.row: 2
            Layout.column: 0
           Layout.columnSpan: 2
            font.family: "Tahoma"
            Material.accent: "#D2691E"
            color: "#ecbe82"

        }



}
}
