#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QNetworkReply>
#include "httpcontroller.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    HttpController httpController;
    QQmlApplicationEngine engine;

    QQmlContext *context = engine.rootContext();
    context->setContextProperty("httpController", &httpController);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QObject* main = engine.rootObjects()[0];
    HttpController sendtoqml(main);
    engine.rootContext()->setContextProperty("_send", &sendtoqml);

    return app.exec();//запуск бесконечного цикла обработки сообщений и слотов/сигналов
}
