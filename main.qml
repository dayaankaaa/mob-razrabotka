import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12
import QtQuick.Dialogs 1.0


ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("Instagram Dayana")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1Form {
        }

        Page2Form {
            id: page2Form
        }
        Page3Form {
            id: page3Form
        }
        Page4Form {
            id: page4Form
        }

        Page5Form {
            id: page5Form
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        Material.accent: "#D2691E"

        TabButton {
            text: qsTr("Lab1")
            Material.accent: "#ecbe82"
        }
        TabButton {
            text: qsTr("Lab2_1")
            Material.accent: "#ecbe82"
        }
        TabButton {
            text: qsTr("Lab2_2")
            Material.accent: "#ecbe82"
        }
        TabButton {
            text: qsTr("Lab3")
            Material.accent: "#ecbe82"
        }
    }
}
